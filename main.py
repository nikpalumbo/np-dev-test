# -*- coding: utf-8 -*-
"""
This modules has been developed by Nicola Palumbo <nikpalumbo@gmail.com>
for a Take Home Dev Test for Rackspace.

It's has been developed and tested with Python 3.4

This module  resolves the problems: q4,q5,q6 therefore accepts
three options and a file to get the input for the problems

For q3:
python main.py q4 myfilename
Output: A string with no space and no duplicates (if are next to each other)

For q4:
python main.py q5 myfilename
Output: a line with a spiral order repr. of a matrix

For q5:
python main.py q6 myfilename
Output: Yes, if a subtree is in a tree
"""

import argparse
import re


class Foo:

    def q4(args):
        """Prints out the string stripped and without any
        duplicate character (next to each other).

        The first parts [ˆ\s] excludes any spaces,
        while the second part selects the duplicate
        char if they are next to each other and will
        be replaced with an empty char.
        """
        res = re.sub(r'[ˆ\s]|(.)(?=\1)', r'', args.filename.read())
        print(res)

    def q5(args):
        """Prints out in spiral order a matrix (nxm).

        This function uses the recursion, basically
        a matrix is broken down in several sub matrix whose
        will be printed the last row from top left to right,
        from top right to bottom and  from bottom right to left.
        """

        a = []
        for line in args.filename:
            line = line.strip()
            a.append(line.split(' '))

        m, n = len(a), len(a[0])


        def print_spiral(a, m, n, k):

            #Base case: matrix with elements
            if m <= 0 and m <= 0:
                return

            #Base case: one dimension matrix
            if m == 1:
                for j in range(n):
                    print(a[k][k+j], end=" ")
                return

            #Base case: one dimension matrix
            if n == 1:
                for i in range(m):
                    print(a[k+i][k], end=" ")
                return

            #TODO: Loops could be remove with proper slicing of a list
            #top left to right
            for j in range(n-1):
                print(a[k][k+j], end=" ")

            #top right to bottom
            for i in range(m-1):
                print(a[k+i][k+n-1], end=" ")

            #bottom right to left
            for j in range(n-1):
                print(a[k+m-1][k+n-1-j], end=" ")

            #bottom left to top
            for i in range(m-1):
                print(a[k+m-1-i][k], end=" ")

            print_spiral(a, m-2,n-2, k+1)

        print_spiral(a, m, n, 0)

    def q6(args):
        """Binary Tree.

        Loads two binary trees from a file in breadth-first heap style representation
        Builds a binary tree, using the class BT and searches if the second subtree is
        in the first tree.

        This functions allows to search if a subtree (also with more nodes than 3) is
        in the first tree.
        """

        tree = args.filename.readline().strip()
        subtree = args.filename.readline().strip()

        tree = [''] + tree.split(',')
        subtree = [''] + subtree.split(',')

        class BT:

            def __init__(self):
                self.left = None
                self.right = None
                self.value = None

            @staticmethod
            def build_tree(array, i, node):
                """Builds a tree from an array
                :param array: an array with a breadth-first heap style representation of a tree
                :param i: the root index node
                :param node: just the root node
                :return: a tree where each node contains tree values (left,right, value)
                """
                if array and i <= len(array) - 1 and array[i]:
                    node.value = array[i]
                    node.left = BT.build_tree(array, i*2,  BT())
                    node.right = BT.build_tree(array, i*2+1, BT())
                return node

            @staticmethod
            def areEqual(node1, node2):
                """Checks if two nodes are equal
                :param node1: a node
                :param node2: a node
                :return: True if nodes are equal
                """

                if node1 is None and node2 is None:
                    return True
                if node1 is None or node2 is None:
                    return False

                if node1.value == node2.value and\
                   BT.areEqual(node1.left, node2.left) and\
                        BT.areEqual(node1.right, node2.right):
                        return True

            @staticmethod
            def isSubTree(t, s):
                """Checks if s is in t
                :param t: main tree
                :param s: sub tree
                :return: True if s is in t, otherwise False
                """
                #TODO: There is a special case, where the value returned is wrong.

                if s is None:
                    return True
                if t is None:
                    return False
                if BT.areEqual(t, s):
                    return True

                value = BT.isSubTree(t.left, s)
                value = BT.isSubTree(t.right, s)
                return value

        t1 = BT()
        t2 = BT()
        BT.build_tree(tree, 1, t1)
        BT.build_tree(subtree, 1, t2)

        if BT.isSubTree(t1,t2):
            print("Yes")

if __name__ == "__main__":


    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='Exercies')

    for ex in ['q4','q5','q6']:
        p = subparsers.add_parser(ex)
        p.set_defaults(func=getattr(Foo, ex))#introspection :)
        p.add_argument('filename', type=argparse.FileType('r'))

    args = parser.parse_args()
    args.func(args)
